package com.bearfishapps.drive.Tools;

public class Constants {
    public static String prefName = "com.bearfishapps.plantaryexpedition.erocs",
            scoreName = "score",
            tycho = "data/tycho___.ttf",
            virgo = "data/virgo.ttf",


            logo = "data/Bearfish.png",
            atlas = "data/pack1.atlas";
}

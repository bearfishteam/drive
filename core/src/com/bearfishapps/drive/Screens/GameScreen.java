package com.bearfishapps.drive.Screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.input.GestureDetector;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.bearfishapps.drive.Drive;
import com.bearfishapps.drive.Services.DriverService;
import com.bearfishapps.drive.Services.IndexingService;
import com.bearfishapps.drive.Services.WeldingService;
import com.bearfishapps.drive.WorldObjects.DrawableObject;
import com.bearfishapps.drive.WorldObjects.Ground;
import com.bearfishapps.drive.WorldObjects.PlayableObjects.Frame.Frame;
import com.bearfishapps.drive.WorldObjects.PlayableObjects.PlayableObject;
import com.bearfishapps.drive.WorldObjects.PlayableObjects.Wheels.DrivableWheel;
import com.bearfishapps.drive.WorldObjects.PlayableObjects.Wheels.Wheel;
import com.bearfishapps.drive.WorldObjects.WorldConstants;

import java.util.ArrayList;

public class GameScreen extends Screens {

    private World world;
    private Ground ground;
    private DriverService driverService;
    private IndexingService indexService;
    private ArrayList<PlayableObject> objects = new ArrayList<PlayableObject>();

    // TODO: REMOVE DEGUB RENDERER DIRNG RELEASE
    private Box2DDebugRenderer debugRenderer;

    public GameScreen(Drive game) {
        super(game);
        // Create World
        world = new World(new Vector2(0, -60f), true);
        // debugRenderer
        debugRenderer = new Box2DDebugRenderer();

        ground = new Ground(world);

        // *NOTE: FRAME OBJECTS FIRST, DRIVE WHEELs next, OTHERs come last
        objects.add(new Frame(world, 2, 8, 14));
        objects.add(new Frame(world, 2, 12, 14));
        objects.add(new Frame(world, 2, 16, 14));
        objects.add(new Frame(world, 2, 20, 14));
        objects.add(new Frame(world, 2, 24, 14));

        objects.add(new DrivableWheel(world, 2, 8, 10, 0));
        objects.add(new DrivableWheel(world, 2, 12, 10, 0));
        objects.add(new DrivableWheel(world, 2, 16, 10, 0));
        objects.add(new DrivableWheel(world, 2, 20, 10, 0));
        objects.add(new DrivableWheel(world, 2, 24, 10, 0));

        objects.add(new DrivableWheel(world, 2, 8, 18, 180));
        objects.add(new DrivableWheel(world, 2, 12, 18, 180));
        objects.add(new DrivableWheel(world, 2, 16, 18, 180));
        objects.add(new DrivableWheel(world, 2, 20, 18, 180));
        objects.add(new DrivableWheel(world, 2, 24, 18, 180));

        objects.add(new Wheel(world, 2, 4, 14, 90));
        objects.add(new Wheel(world, 2, 28, 14, -90));

        indexService = new IndexingService(objects);
        indexService.getOrganizedObjects();
        WeldingService.WeldVehicle(world, indexService.getOrganizedObjects());
        driverService = new DriverService(WorldConstants.torque, WorldConstants.speed, indexService.getINDEX_DriveWheelStart(), indexService.getINDEX_DriveWheelEnd(), objects);

        for (PlayableObject obj : objects) {
            Gdx.app.log("object", obj.toString());
        }

        camera.zoom = 0.1f;
        camera.update();
    }

    @Override
    public void draw(float delta, float animationKeyFrame) {
        batch.begin();
        batch.enableBlending();
        for(DrawableObject o: objects) {
            o.draw(batch, shapeRenderer);
        }
        batch.end();

        shapeRenderer.begin(ShapeRenderer.ShapeType.Line);
        ground.draw(batch, shapeRenderer);
        shapeRenderer.end();

//        debugRenderer.render(world, camera.combined);
        world.step(1 / 60f, 6, 2);

        camera.position.set(objects.get(indexService.getINDEX_CenterObject()).getPosition().x,
                objects.get(indexService.getINDEX_CenterObject()).getPosition().y, 0);
        camera.update();

    }

    @Override
    public void preShow(Table table, InputMultiplexer multiplexer) {

        multiplexer.addProcessor(new GestureDetector(new GestureDetector.GestureListener() {
            @Override
            public boolean touchDown(float x, float y, int pointer, int button) {

                return false;
            }

            @Override
            public boolean tap(float x, float y, int count, int button) {
                return false;
            }

            @Override
            public boolean longPress(float x, float y) {
                return false;
            }

            @Override
            public boolean fling(float velocityX, float velocityY, int button) {
                return false;
            }

            @Override
            public boolean pan(float x, float y, float deltaX, float deltaY) {
                camera.update();
                return false;
            }

            @Override
            public boolean panStop(float x, float y, int pointer, int button) {
                return false;
            }

            @Override
            public boolean zoom(float initialDistance, float distance) {
                return false;
            }

            @Override
            public boolean pinch(Vector2 initialPointer1, Vector2 initialPointer2, Vector2 pointer1, Vector2 pointer2) {
                return false;
            }
        }));

        multiplexer.addProcessor(new InputProcessor() {
            @Override
            public boolean keyDown(int keycode) {
                if (Input.Keys.LEFT == keycode) {
                    driverService.driveInv();
                } else if (Input.Keys.UP == keycode) {
                } else if (Input.Keys.DOWN == keycode) {

                } else if (Input.Keys.RIGHT == keycode) {
                    driverService.drive();
                }

                return false;
            }

            @Override
            public boolean keyUp(int keycode) {
                driverService.stop();
                return false;
            }

            @Override
            public boolean keyTyped(char character) {
                return false;
            }

            @Override
            public boolean touchDown(int screenX, int screenY, int pointer, int button) {
                return false;
            }

            @Override
            public boolean touchUp(int screenX, int screenY, int pointer, int button) {
                return false;
            }

            @Override
            public boolean touchDragged(int screenX, int screenY, int pointer) {
                return false;
            }

            @Override
            public boolean mouseMoved(int screenX, int screenY) {
                return false;
            }

            @Override
            public boolean scrolled(int amount) {
                camera.zoom += ((float) amount) / 20;
                camera.update();
                return false;
            }
        });
    }

    @Override
    public void destroy() {
    }
}

package com.bearfishapps.drive.Screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.bearfishapps.drive.Drive;
import com.bearfishapps.drive.Services.TextureRegionService;
import com.bearfishapps.drive.Tools.CustomClasses.CustomButton;
import com.bearfishapps.drive.Tools.CustomClasses.CustomImageButton;
import com.bearfishapps.drive.Tools.CustomClasses.CustomLabel;

public class MainMenuScreen extends Screens {
    private Label title;
    private ImageButton playButton, quitButton;

    public MainMenuScreen(Drive game) {
        super(game);

        CustomLabel.make(48, Color.WHITE);
        title = new Label("Planetary\nExpedition", CustomLabel.style);


        CustomImageButton.make(TextureRegionService.playButton);
        playButton = new ImageButton(CustomImageButton.style);
        CustomImageButton.make(TextureRegionService.quitButton);
        quitButton = new ImageButton(CustomImageButton.style);

    }

    @Override
    public void draw(float delta, float animationKeyFrame) {

    }

    @Override
    public void preShow(Table table, InputMultiplexer multiplexer) {
        playButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                game.setScreen(new GameScreen(game));
            }
        });

        quitButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                Gdx.app.exit();
            }
        });

        table.setDebug(true);
        table.top().right();
        table.add(title).pad(20).row();
        table.add(playButton).pad(10).width(100).height(100).right().row();
        table.add(quitButton).pad(10).width(100).height(100).right().row();

    }

    @Override
    public void destroy() {
        CustomLabel.dispose();
        CustomButton.dispose();
    }
}

package com.bearfishapps.drive.Screens;

import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.Scaling;
import com.bearfishapps.drive.Drive;
import com.bearfishapps.drive.Services.TextureRegionService;
import com.bearfishapps.drive.Tools.Constants;

public class SplashScreen extends Screens {
    public SplashScreen(Drive game) {
        super(game);

        game.getLoader().loadEverything();
    }

    @Override
    public void draw(float delta, float animationKeyFrame) {
        if (game.getLoader().get().update() && animationKeyFrame >= 2) {
            TextureRegionService.mapAll(game.getLoader().get().get(Constants.atlas, TextureAtlas.class));
            game.setScreen(new MainMenuScreen(game));
        }
    }

    @Override
    public void preShow(Table table, InputMultiplexer multiplexer) {
        Image logo = new Image(game.getLoader().get().get(Constants.logo, Texture.class));
        logo.setScaling(Scaling.fit);

        table.setDebug(true);
        table.bottom().left();
        table.add(logo).width(300).height(200).fill().row();
    }

    @Override
    public void destroy() {

    }
}

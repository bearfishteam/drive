package com.bearfishapps.drive.Screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.viewport.ExtendViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.bearfishapps.drive.Drive;

////////This is a custom class made for simplifying the process of making a screen

public abstract class Screens implements com.badlogic.gdx.Screen {
    // needed for screen changes
    protected Drive game;
    // default background color (black) 0/255f for all paremeters
    private int color[] = {0, 0, 0, 0};
    // keyframe for 2d animation
    private float keyFrame = 0;

    // camera and ui stuff
    protected OrthographicCamera camera;
    protected Viewport viewport;
    protected Stage stage;
    protected Table table;

    // used to render sprites and shapes
    protected SpriteBatch batch;
    protected ShapeRenderer shapeRenderer;

    // constructor with default screen size;
    public Screens(Drive game) {
        this.game = game;

        camera = new OrthographicCamera();
        viewport = new ExtendViewport(800, 480, camera);
        camera.position.set(camera.viewportWidth / 2, camera.viewportHeight / 2, 0);

        stage = new Stage(viewport);
        table = new Table();

        batch = new SpriteBatch();
        shapeRenderer = new ShapeRenderer();
        camera.update();
    }

    // constructor with variable screen size;
    public Screens(Drive game, int sizeX, int sizeY) {
        this.game = game;

        camera = new OrthographicCamera();
        viewport = new ExtendViewport(sizeX, sizeY, camera);
        camera.position.set(camera.viewportWidth / 2, camera.viewportHeight / 2, 0);
        camera.update();

        stage = new Stage(viewport);
        table = new Table();

        batch = new SpriteBatch();
        shapeRenderer = new ShapeRenderer();
        camera.update();
    }

    // change the background color to (rgb values must be between 0 and 255, and alpha must be between 0 and 100)
    protected void setBackgroundColor(int r, int g, int b, int a) {
        color[0] = r;
        color[1] = g;
        color[2] = b;
        color[3] = a;
    }

    @Override
    public void show() {
        // for input detection
        InputMultiplexer multiplexer = new InputMultiplexer();

        // abstract class in order to do custom settings
        preShow(table, multiplexer);

        // UI and interaction stuff
        table.setFillParent(true);
        stage.addActor(table);
        stage.addAction(Actions.sequence(Actions.alpha(0), Actions.fadeIn(1f)));
        multiplexer.addProcessor(stage);
        Gdx.input.setInputProcessor(multiplexer);
    }

    @Override
    public void render(float delta) {
        keyFrame += delta;
        // flush everything on screen
        Gdx.gl.glClearColor(color[0] / 255.0f, color[1] / 255.0f, color[2] / 255.0f, color[3] / 100f);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        // set the drawing to the relative coordinates of the camera
        batch.setProjectionMatrix(camera.combined);
        shapeRenderer.setProjectionMatrix(camera.combined);
        // for drawing
        draw(delta, keyFrame);

        // for ui
        stage.act();
        stage.draw();
    }

    @Override
    public void resize(int width, int height) {
        // resize camera
        viewport.update(width, height, true);
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        stage.dispose();
        destroy();
    }

    // abstract classes for customization
    public abstract void draw(float delta, float animationKeyFrame);

    public abstract void preShow(Table table, InputMultiplexer multiplexer);

    public abstract void destroy();
}

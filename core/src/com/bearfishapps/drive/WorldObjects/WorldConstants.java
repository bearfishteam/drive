package com.bearfishapps.drive.WorldObjects;

public class WorldConstants {
    public static float
            gravity = -9.8f,
            friction = 15f,

    torque = 690,
            speed = 369,

    wheelDensity = 2f,
            frameDensity = 2f;

    public static short
            DEFAULT_BITS = 0x01,
            WHEELS_BITS = 0x02,
            TRIANGLE_BITS = 0x03,
            FRAME_BITS = 0x04;

}

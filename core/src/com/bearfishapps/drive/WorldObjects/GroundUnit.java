package com.bearfishapps.drive.WorldObjects;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.EdgeShape;
import com.badlogic.gdx.physics.box2d.World;

public class GroundUnit implements DrawableObject{
    private Body body;
    private Vector2 one, two;

    public GroundUnit(World world, Vector2 one, Vector2 two) {
        this.one = one;
        this.two = two;
        // Create our body definition
        BodyDef groundBodyDef = new BodyDef();
// Set its world position
        groundBodyDef.position.set(new Vector2(0, 0));

// Create a body from the defintion and add it to the world
        body = world.createBody(groundBodyDef);

        EdgeShape ground = new EdgeShape();
        ground.set(one, two);
// Create a fixture from our polygon shape and add it to our ground body
        body.createFixture(ground, 0.0f);
// Clean up after ourselves
        ground.dispose();
    }

    public Body getBody() {
        return body;
    }

    @Override
    public void draw(SpriteBatch batch, ShapeRenderer renderer) {
        renderer.line(one, two);
    }
}

package com.bearfishapps.drive.WorldObjects.PlayableObjects;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.World;
import com.bearfishapps.drive.WorldObjects.DrawableObject;

public abstract class PlayableObject implements DrawableObject{
    protected float SCALE, x, y;
    protected World world;

    public PlayableObject(World world, float scale, float x, float y) {
        this.world = world;
        SCALE = scale;
        this.x = x;
        this.y = y;
    }

    public abstract Body[] getAttachableBodies();

    public abstract Vector2[] getAttachablePositions();

    public abstract int getNumberOfAttachablePositions();

    public abstract Vector2 getPosition();

    @Override
    public String toString() {
        return "[" + x + ", " + y + "]";
    }
}

package com.bearfishapps.drive.WorldObjects.PlayableObjects.Frame;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.World;
import com.bearfishapps.drive.Services.TextureRegionService;
import com.bearfishapps.drive.WorldObjects.PlayableObjects.PlayableObject;
import com.bearfishapps.drive.WorldObjects.WorldConstants;
import com.bearfishapps.drive.WorldObjects.WorldUtils;

public class Frame extends PlayableObject {
    private float[] frameV = {-1, -1, 1, -1, 1, 1, -1, 1};
    private Body frame;

    public Frame(World world, float scale, float x, float y) {
        super(world, scale, x, y);

        frameV = WorldUtils.scaleF(frameV, scale);

        frame = WorldUtils.createPoly(world, BodyDef.BodyType.DynamicBody, frameV, x, y, WorldConstants.frameDensity, 0, WorldConstants.friction, WorldConstants.FRAME_BITS, (short) (WorldConstants.FRAME_BITS | WorldConstants.DEFAULT_BITS));
    }

    @Override
    public Body[] getAttachableBodies() {
        return new Body[]{frame, frame, frame, frame};
    }

    @Override
    public Vector2[] getAttachablePositions() {
        // DOWN, RIGHT, UP, LEFT
        return new Vector2[]{new Vector2(0, -1 * SCALE), new Vector2(1 * SCALE, 0), new Vector2(0, 1 * SCALE), new Vector2(-1 * SCALE, 0)};
    }

    @Override
    public int getNumberOfAttachablePositions() {
        return 4;
    }

    @Override
    public Vector2 getPosition() {
        return frame.getPosition();
    }

    @Override
    public void draw(SpriteBatch batch, ShapeRenderer renderer) {
        batch.draw(TextureRegionService.frame, frame.getPosition().x-SCALE, frame.getPosition().y-SCALE,
                SCALE, SCALE,SCALE*2, SCALE*2,1,1, MathUtils.radiansToDegrees*frame.getAngle());
    }
}

package com.bearfishapps.drive.WorldObjects.PlayableObjects.Wheels;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.physics.box2d.joints.RevoluteJoint;
import com.badlogic.gdx.physics.box2d.joints.RevoluteJointDef;
import com.bearfishapps.drive.Services.TextureRegionService;
import com.bearfishapps.drive.WorldObjects.PlayableObjects.PlayableObject;
import com.bearfishapps.drive.WorldObjects.WorldConstants;
import com.bearfishapps.drive.WorldObjects.WorldUtils;

public class Wheel extends PlayableObject {
    private float wheelRadius = 0.69f, rotation = 0;
    private float[] barV = {0f, 0.2f, 0.91f, 1, -0.91f, 1};
    private Body wheel, bar;

    protected RevoluteJoint revoluteJoint;

    public Wheel(World world, float scale, float x, float y, float rotation) {
        super(world, scale, x, y);

        this.rotation = rotation;
        barV = WorldUtils.rotateF(barV, rotation);
        barV = WorldUtils.scaleF(barV, scale);

        wheelRadius *= scale;

//        this.x = this.x*scale;
//        this.y = this.y*scale;

        wheel = WorldUtils.createCircleBody(world, BodyDef.BodyType.DynamicBody, this.x, this.y, WorldConstants.wheelDensity, 0, WorldConstants.friction, wheelRadius, WorldConstants.WHEELS_BITS, WorldConstants.DEFAULT_BITS);
        bar = WorldUtils.createPoly(world, BodyDef.BodyType.DynamicBody, barV, x, y, WorldConstants.wheelDensity, 0, WorldConstants.friction, WorldConstants.TRIANGLE_BITS, (short) (WorldConstants.DEFAULT_BITS | WorldConstants.FRAME_BITS));

        RevoluteJointDef revoluteJointDef = new RevoluteJointDef();
        revoluteJointDef.bodyA = wheel;
        revoluteJointDef.bodyB = bar;
        revoluteJointDef.localAnchorA.set(0, 0);
        revoluteJointDef.localAnchorB.set(bar.getLocalCenter().x - (2f / 3f * SCALE) * MathUtils.sinDeg(rotation), bar.getLocalCenter().y - (2f / 3f * SCALE) * MathUtils.cosDeg(rotation));//0.5f*SCALE,0.475f*SCALE);
        revoluteJointDef.collideConnected = false;
        revoluteJointDef.enableMotor = true;
        revoluteJointDef.maxMotorTorque = 1000f;
        revoluteJoint = (RevoluteJoint) world.createJoint(revoluteJointDef);
    }

    @Override
    public Body[] getAttachableBodies() {
        return new Body[]{bar};
    }

    @Override
    public Vector2[] getAttachablePositions() {
        float x = SCALE * MathUtils.sinDeg(rotation);
        float y = SCALE * MathUtils.cosDeg(-rotation);
        return new Vector2[]{new Vector2(x, y)};
    }

    @Override
    public int getNumberOfAttachablePositions() {
        return 1;
    }

    @Override
    public Vector2 getPosition() {
        return wheel.getPosition();
    }

    public float getRotation() {
        return rotation;
    }

    @Override
    public void draw(SpriteBatch batch, ShapeRenderer renderer) {
        batch.draw(TextureRegionService.cogWheel2, wheel.getPosition().x-wheelRadius, wheel.getPosition().y-wheelRadius,
                wheelRadius, wheelRadius,wheelRadius*2, wheelRadius*2,1,1, MathUtils.radiansToDegrees*wheel.getAngle());
    }
}

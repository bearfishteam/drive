package com.bearfishapps.drive.WorldObjects.PlayableObjects.Wheels;

import com.badlogic.gdx.physics.box2d.World;
import com.bearfishapps.drive.WorldObjects.PlayableObjects.Drivable;

public class DrivableWheel extends Wheel implements Drivable {
    public DrivableWheel(World world, float scale, float x, float y, float rotation) {
        super(world, scale, x, y, rotation);
    }

    @Override
    public void applyThrottle(float torque, float speed) {
        revoluteJoint.setMaxMotorTorque(torque * 2 * SCALE);
        revoluteJoint.setMotorSpeed(-speed);
        revoluteJoint.setMaxMotorTorque(torque * 2 * SCALE);
        revoluteJoint.setMotorSpeed(-speed);
    }

    @Override
    public void applyZero() {
        revoluteJoint.setMaxMotorTorque(0);
        revoluteJoint.setMotorSpeed(0);
        revoluteJoint.setMaxMotorTorque(0);
        revoluteJoint.setMotorSpeed(0);
    }
}

package com.bearfishapps.drive.WorldObjects.PlayableObjects;

public interface Drivable {
    public void applyThrottle(float torque, float speed);

    public void applyZero();

}

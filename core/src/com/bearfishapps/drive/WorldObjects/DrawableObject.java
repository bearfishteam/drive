package com.bearfishapps.drive.WorldObjects;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;

public interface DrawableObject {

    void draw(SpriteBatch batch, ShapeRenderer renderer);
}

package com.bearfishapps.drive.WorldObjects;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.World;

public class Ground implements DrawableObject{
    private GroundUnit[] groundPieces;

    public Ground(World world) {
        groundPieces = new GroundUnit[1000];
        groundPieces[0] = new GroundUnit(world, new Vector2(-100, 300), new Vector2(-100, 0));
        int prevX = -100, prevY = 0;

        for (int i = 1; i < groundPieces.length; i++) {
            int newY = i > 10 ? prevY + MathUtils.random(-10, 10) : prevY;
            int newX = prevX + 20;
            groundPieces[i] = new GroundUnit(world, new Vector2(prevX, prevY), new Vector2(newX, newY));
            prevX = newX;
            prevY = newY;
        }

    }

    public GroundUnit[] getGroundPieces() {
        return groundPieces;
    }

    @Override
    public void draw(SpriteBatch batch, ShapeRenderer renderer) {
        for(GroundUnit g: groundPieces) {
            g.draw(batch, renderer);
        }
    }
}

package com.bearfishapps.drive;

import com.badlogic.gdx.Game;
import com.bearfishapps.drive.Screens.SplashScreen;
import com.bearfishapps.drive.Tools.AssetLoader;

public class Drive extends Game {
	private AssetLoader loader;

	@Override
	public void create() {
		loader = new AssetLoader();
		loader.loadLogo();

		loader.get().finishLoading();
		this.setScreen(new SplashScreen(this));
	}

	public AssetLoader getLoader() {
		return loader;
	}
}

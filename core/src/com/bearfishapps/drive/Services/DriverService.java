package com.bearfishapps.drive.Services;

import com.bearfishapps.drive.WorldObjects.PlayableObjects.Drivable;
import com.bearfishapps.drive.WorldObjects.PlayableObjects.PlayableObject;

import java.util.ArrayList;

public class DriverService {
    private float torque, speed;
    private ArrayList<PlayableObject> objects;
    private int INDEX_DriveWheelStart, INDEX_DriveWheelEnd;

    public DriverService(float t, float s, int INDEX_DriveWheelStart, int INDEX_DriveWheelEnd, ArrayList<PlayableObject> objects) {
        torque = t;
        speed = s;
        this.INDEX_DriveWheelStart = INDEX_DriveWheelStart;
        this.INDEX_DriveWheelEnd = INDEX_DriveWheelEnd;
        this.objects = objects;
    }

    public void drive() {
        for (int i = INDEX_DriveWheelStart; i <= INDEX_DriveWheelEnd; i++) {
            applyThrottle(((Drivable) objects.get(i)), -1);
        }
    }

    public void driveInv() {
        for (int i = INDEX_DriveWheelStart; i <= INDEX_DriveWheelEnd; i++) {
            applyThrottle(((Drivable) objects.get(i)), 1);
        }
    }

    public void stop() {
        for (int i = INDEX_DriveWheelStart; i <= INDEX_DriveWheelEnd; i++) {
            noThrottle(((Drivable) objects.get(i)));
        }
    }

    private void applyThrottle(Drivable drivableObject, int modifier) {
        drivableObject.applyThrottle(torque, modifier * speed);
    }

    private void noThrottle(Drivable drivableObject) {
        drivableObject.applyThrottle(0, 0);
    }

}

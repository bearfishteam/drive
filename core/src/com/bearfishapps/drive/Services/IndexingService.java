package com.bearfishapps.drive.Services;

import com.bearfishapps.drive.WorldObjects.PlayableObjects.PlayableObject;
import com.bearfishapps.drive.WorldObjects.PlayableObjects.Wheels.DrivableWheel;

import java.util.ArrayList;

public class IndexingService {
    private PlayableObject[][] organizedObjects;
    private int INDEX_DriveWheelStart = -1, INDEX_DriveWheelEnd = -1, INDEX_CenterObject = -1;

    public IndexingService(ArrayList<PlayableObject> objects) {
        float top = -9999, bottom = 9999, left = 9999, right = -9999;
        for (int i = 0; i < objects.size(); i++) {
            if (i == 0) {
                top = bottom = objects.get(i).getPosition().y;
                left = right = objects.get(i).getPosition().x;
            } else {
                if (top < objects.get(i).getPosition().y)
                    top = objects.get(i).getPosition().y;
                if (bottom > objects.get(i).getPosition().y)
                    bottom = objects.get(i).getPosition().y;
                if (left > objects.get(i).getPosition().x)
                    left = objects.get(i).getPosition().x;
                if (right < objects.get(i).getPosition().x)
                    right = objects.get(i).getPosition().x;
            }

            if (objects.get(i) instanceof DrivableWheel) {
                if (INDEX_DriveWheelStart == -1)
                    INDEX_DriveWheelStart = i;
                else
                    INDEX_DriveWheelEnd = i;
            }
        }

        organizedObjects = new PlayableObject[(int) (((top - bottom) / 4) + 1)][(int) (((right - left) / 4) + 1)];
        float centerY = (top+bottom)/2, centerX = (left+right)/2;
        int i = 0;
        for (PlayableObject o : objects) {
            if(o.getPosition().x == centerX && o.getPosition().y == centerY) {
                INDEX_CenterObject = i;
            }
            i++;

            int y = 0, x = 0;
            while (true) {
                if (organizedObjects[y][x] == null) {
                    if (y == 0 && x == 0) {
                        if (x + 1 <= organizedObjects[0].length && organizedObjects[y][x + 1] != null &&
                                organizedObjects[y][x + 1].getPosition().y != o.getPosition().y) {
                            y++;
                            continue;
                        } else if (y + 1 <= organizedObjects.length && organizedObjects[y + 1][x] != null &&
                                organizedObjects[y + 1][x].getPosition().x != o.getPosition().x) {
                            x++;
                            continue;
                        } else {
                            organizedObjects[y][x] = o;
                            break;
                        }
                    } else {
                        organizedObjects[y][x] = o;
                        break;
                    }
                } else {
                    if (organizedObjects[y][x].getPosition().x < o.getPosition().x) {
                        x++;
                        continue;
                    }
                    if (organizedObjects[y][x].getPosition().y > o.getPosition().y) {
                        y++;
                        continue;
                    }
                    if (organizedObjects[y][x].getPosition().x > o.getPosition().x) {
                        shift(true, false);
                        continue;
                    }
                    if (organizedObjects[y][x].getPosition().y < o.getPosition().y) {
                        shift(false, true);
                        continue;
                    }
                }
            }
        }
    }

    private void shift(boolean xx, boolean yy) {
        if (xx) {
            for (int y = 0; y < organizedObjects.length; y++) {
                for (int x = organizedObjects[y].length - 1; x >= 0; x--) {
                    if (x != 0)
                        organizedObjects[y][x] = organizedObjects[y][x - 1];
                    else
                        organizedObjects[y][x] = null;
                }
            }
        }
        if (yy) {
            for (int y = organizedObjects.length - 1; y >= 0; y--) {
                for (int x = 0; x < organizedObjects[y].length; x++) {
                    if (y != 0)
                        organizedObjects[y][x] = organizedObjects[y - 1][x];
                    else
                        organizedObjects[y][x] = null;
                }
            }
        }
    }

    public PlayableObject[][] getOrganizedObjects() {
        return organizedObjects;
    }

    public int getINDEX_DriveWheelStart() {
        return INDEX_DriveWheelStart;
    }

    public int getINDEX_DriveWheelEnd() {
        return INDEX_DriveWheelEnd;
    }

    public int getINDEX_CenterObject() {
        return INDEX_CenterObject;
    }
}

package com.bearfishapps.drive.Services;

import com.badlogic.gdx.physics.box2d.World;
import com.bearfishapps.drive.WorldObjects.PlayableObjects.Frame.Frame;
import com.bearfishapps.drive.WorldObjects.PlayableObjects.PlayableObject;
import com.bearfishapps.drive.WorldObjects.PlayableObjects.Wheels.Wheel;
import com.bearfishapps.drive.WorldObjects.WorldUtils;

public class WeldingService {
    public static void WeldVehicle(World world, PlayableObject[][] objects) {
        for (int y = 0; y < objects.length; y++) {
            for (int x = 0; x < objects[y].length; x++) {
                if (objects[y][x] instanceof Frame) {
                    if (x + 1 < objects[y].length && objects[y][x + 1] instanceof Frame)
                        WorldUtils.weldJoint(world, objects[y][x].getAttachableBodies()[1], objects[y][x + 1].getAttachableBodies()[3],
                                objects[y][x].getAttachablePositions()[1], objects[y][x + 1].getAttachablePositions()[3]);
                    if (y + 1 < objects.length && objects[y + 1][x] instanceof Frame)
                        WorldUtils.weldJoint(world, objects[y][x].getAttachableBodies()[0], objects[y + 1][x].getAttachableBodies()[2],
                                objects[y][x].getAttachablePositions()[0], objects[y + 1][x].getAttachablePositions()[2]);
                }
                if (objects[y][x] instanceof Wheel) {
                    float rotation = ((Wheel) objects[y][x]).getRotation();
                    if (rotation == 0 && y - 1 >= 0 && objects[y - 1][x] instanceof Frame) {
                        WorldUtils.weldJoint(world, objects[y][x].getAttachableBodies()[0], objects[y - 1][x].getAttachableBodies()[0],
                                objects[y][x].getAttachablePositions()[0], objects[y - 1][x].getAttachablePositions()[0]);
                    } else if (rotation == 90 && x + 1 <= objects[y].length && objects[y][x + 1] instanceof Frame) {
                        WorldUtils.weldJoint(world, objects[y][x].getAttachableBodies()[0], objects[y][x + 1].getAttachableBodies()[3],
                                objects[y][x].getAttachablePositions()[0], objects[y][x + 1].getAttachablePositions()[3]);
                    } else if (rotation == 180 && y + 1 <= objects.length && objects[y + 1][x] instanceof Frame) {
                        WorldUtils.weldJoint(world, objects[y][x].getAttachableBodies()[0], objects[y + 1][x].getAttachableBodies()[2],
                                objects[y][x].getAttachablePositions()[0], objects[y + 1][x].getAttachablePositions()[2]);
                    } else if (rotation == -90 && x - 1 >= 0 && objects[y][x - 1] instanceof Frame) {
                        WorldUtils.weldJoint(world, objects[y][x].getAttachableBodies()[0], objects[y][x - 1].getAttachableBodies()[1],
                                objects[y][x].getAttachablePositions()[0], objects[y][x - 1].getAttachablePositions()[1]);
                    }

                }
            }
        }
    }
}
